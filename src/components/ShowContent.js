import React, { Component } from 'react';
import { Columns } from 'react-bulma-components';
import { Notification,Button } from 'reactbulma'
import List from './List';
import InputField from './InputField';

class ShowContent extends Component {

  constructor(props) {
    super(props);
    this.state = {
        data : [],
        status: 0
    }
    this.reload = this.reload.bind(this);
  }

  reload = () => {
    window.location.reload(); 
  }

  componentWillReceiveProps(prevProps, prevState) {
        this.setState({
            data: prevProps.data,
            status: prevProps.status
        });
    }

  render() {
    const { data, status } = this.state;
    
    let items = typeof(data) === "object" ? data.data:[];

    return (
      <div className="ShowContent">
        <InputField {...this.props} />
       {
          status === 401 ?
          <div className="err-r" style={{marginTop:'1rem'}}>
          <Notification danger> 
            { data } 
            <br />
            
          </Notification><Button onClick={this.reload}> โหลดหน้าใหม่ </Button></div>:""
        }
        <Columns breakpoint="mobile" className="is-marginless">
            {
               status !== 200 ? "":
              items.map(function(item, i){
                return <List key={"div-" + i} {...item} />
              })
            }
        </Columns>
      </div>
    );
  }
}

ShowContent.defaultProps = { data: [], status: 0}

export default ShowContent;
