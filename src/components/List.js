import React, { Component } from 'react';
import { Columns,Image,Notification } from 'react-bulma-components';
import './App.css';


class List extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data:[]
    }
  }

  render() {
    const { name, url } = this.props;

    return (
        <Columns.Column mobileSize={6} tabletSize={4} widescreenSize={3}>
            <Image src={ "http://localhost:8000/" + url} size="16by9" />
            <Notification>{name}</Notification> 
        </Columns.Column>
    );
  }
}

export default List;
