import React, { Component } from 'react';
import { Container,Notification, Title, SubTitle } from 'reactbulma'
import axios from 'axios';
import ShowContent from './ShowContent';
import './App.css';


class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data:[],
      status: 0,
      number: 0
    }
  }


  handleChange(e) {
    this.setState(
      {number : e.target.value}
    )
  }

  getJSON(){
    axios
      .get('http://localhost:8000/api/' + this.state.number )
      .then(
        (res) => {
          console.log(res);
          this.setState({ data: res.data, status: res.status});
        } 
      )
      .catch((err) => { 
        console.log(err.response.data);
        this.setState({ data: err.response.data, status: err.response.status})
      })
  }

  render() {

    return (
      <div className="App">
        <Container>
          <Notification>
            <Title> Test-02 </Title>
            <SubTitle>Systemswecare, React, Axios</SubTitle>
          </Notification>
          <ShowContent {...this.state} get={this.getJSON.bind(this)} handleChange={this.handleChange.bind(this)}  />
        </Container>
      </div>
    );
  }
}

export default App;
